import 'package:flutter/material.dart';
import 'package:mtn_module_3/appNameToCapitals.dart';

void main() => runApp(const WinningApp());

class WinningApp extends StatelessWidget {
  const WinningApp({super.key});

  @override
  Widget build(BuildContext context) {
    const appTitle = 'Winning App';

    return MaterialApp(
      title: appTitle,
      home: Scaffold(
        appBar: AppBar(
          title: const Text(appTitle),
        ),
        body: const MyCustomForm(),
      ),
    );
  }
}

//Define a custom form widget
class MyCustomForm extends StatefulWidget {
  const MyCustomForm({super.key});

  @override
  MyCustomFormState createState() {
    return MyCustomFormState();
  }
}

class AppList {
  String year, appName;

  AppList({required this.year, required this.appName});
}

class MyCustomFormState extends State<MyCustomForm> {
  final _formKey = GlobalKey<FormState>();
  final myController = TextEditingController(); // Text input for Year
  final myController1 = TextEditingController(); // Text input for App name
  List<AppList> data = [];

  @override
  void dispose() {
    final myController = TextEditingController();
    final myController1 = TextEditingController();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Form(
        key: _formKey,
        child: Column(
          children: <Widget>[
            // Row for aligning fields horizontally.
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Expanded(
                  child: TextFormField(
                    controller: myController,
                    // The validator receives the text that the user has entered.
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Please enter a the year for the winning App';
                      }
                      return null;
                    },
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Year',
                    ),
                  ),
                )
              ],
            ),

            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Expanded(
                  child: TextFormField(
                    controller: myController1,
                    // The validator receives the text that the user has entered.
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Please enter a winning App for MTN Business App of the Year Awards';
                      }
                      return null;
                    },
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'MTN App Name',
                    ),
                  ),
                )
              ],
            ),

            Container(
                padding: const EdgeInsets.all(20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Expanded(
                        child: ElevatedButton(
                      onPressed: () {
                        if (_formKey.currentState!.validate()) {
                          // Add App of the Year into AppList string array
                          data.add(AppList(
                              year: myController.text,
                              appName: myController1.text));
                          data.forEach((element) {
                            print(element.appName);
                            print(element.year);
                            print(data.length);
                          });
                        }
                      },
                      child: const Text('Submit'),
                    )),
                    Expanded(
                        child: ElevatedButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: const Text('Go Back'),
                    )),
                    Expanded(
                        child: ElevatedButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const AppNameToCapitals()),
                        );
                      },
                      child: const Text('Next screen'),
                    )),
                  ],
                )),
          ],
        ));
  }
}
