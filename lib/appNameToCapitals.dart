import 'package:flutter/material.dart';

void main() => runApp(const AppNameToCapitals());

Widget titleSection = Container(
  padding: const EdgeInsets.all(32),
);

class AppNameToCapitals extends StatelessWidget {
  const AppNameToCapitals({super.key});

  @override
  Widget build(BuildContext context) {
    const appTitle = 'App Name To Capitals';

    return MaterialApp(
      title: appTitle,
      home: Scaffold(
        appBar: AppBar(
          title: const Text(appTitle),
        ),
      ),
    );
  }
}
